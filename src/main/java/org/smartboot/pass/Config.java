package org.smartboot.pass;

import org.smartboot.http.common.utils.StringUtils;
import org.smartboot.socket.buffer.BufferPagePool;

import java.io.IOException;
import java.nio.channels.AsynchronousChannelGroup;
import java.util.Base64;
import java.util.concurrent.ThreadFactory;

/**
 * @author 三刀（zhengjunweimail@163.com）
 * @version V1.0 , 2021/7/15
 */
public class Config {
    private String basic;
    private AsynchronousChannelGroup proxyChanelGroup;
    private String version;
    private int port;
    private int readBufferSize;
    private int writeBufferSize;
    private int threadNum;
    private String proxyHost;
    private int proxyPort;
    private String proxyUserName;
    private String proxyPassword;

    private String basicUserName;
    private String basicPassword;

    private BufferPagePool bufferPagePool;

    private BufferPagePool httpClientReadBufferPagePool;

    public void init() throws IOException {
        proxyChanelGroup = AsynchronousChannelGroup.withFixedThreadPool(16, new ThreadFactory() {
            int i = 0;

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "proxyClient-" + (++i));
            }
        });
        if (StringUtils.isNotBlank(basicUserName) && StringUtils.isNotBlank(basicPassword)) {
            this.basic = "Basic " + Base64.getEncoder().encodeToString((basicUserName + ":" + basicPassword).getBytes());
        } else {
            this.basic = null;
        }
        bufferPagePool = new BufferPagePool(1024 * 1024 * 5, threadNum, true);
        httpClientReadBufferPagePool = new BufferPagePool(1024 * 1024 * 5, threadNum, false);
    }

    public String getBasic() {
        return basic;
    }

    public AsynchronousChannelGroup getProxyChanelGroup() {
        return proxyChanelGroup;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public String getProxyUserName() {
        return proxyUserName;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public int getPort() {
        return port;
    }

    public int getReadBufferSize() {
        return readBufferSize;
    }

    public int getWriteBufferSize() {
        return writeBufferSize;
    }

    public int getThreadNum() {
        return threadNum;
    }

    public String getVersion() {
        return version;
    }

    public BufferPagePool getBufferPagePool() {
        return bufferPagePool;
    }

    public BufferPagePool getHttpClientReadBufferPagePool() {
        return httpClientReadBufferPagePool;
    }
}
