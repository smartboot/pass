package org.smartboot.pass.config;

import com.google.gson.annotations.SerializedName;

/**
 * @author 三刀（zhengjunweimail@163.com）
 * @version V1.0 , 2023/2/28
 */
public class PassConfig {
    /**
     * 核心线程数
     */
    @SerializedName("core_thread")
    private int coreThread;
    /**
     * http服务配置
     */
    private HttpConfig http;

    public int getCoreThread() {
        return coreThread;
    }

    public void setCoreThread(int coreThread) {
        this.coreThread = coreThread;
    }

    public HttpConfig getHttp() {
        return http;
    }

    public void setHttp(HttpConfig http) {
        this.http = http;
    }


}
