package org.smartboot.pass.config;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HttpServer {
    /**
     * 监听端口号
     */
    private int port;

    /**
     * 绑定网卡IP
     */
    private String ip = "";
    @SerializedName("server_name")
    private String serverName;
    private List<HttpServerLocation> location;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public List<HttpServerLocation> getLocation() {
        return location;
    }

    public void setLocation(List<HttpServerLocation> location) {
        this.location = location;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}