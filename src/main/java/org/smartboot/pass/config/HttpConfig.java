package org.smartboot.pass.config;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HttpConfig {
    private String include;
    @SerializedName("default_type")
    private String defaultType;

    @SerializedName("keepalive_timeout")
    private int keepaliveTimeout;
    private boolean gzip;
    private List<HttpServer> server;

    public String getInclude() {
        return include;
    }

    public void setInclude(String include) {
        this.include = include;
    }

    public String getDefaultType() {
        return defaultType;
    }

    public void setDefaultType(String defaultType) {
        this.defaultType = defaultType;
    }

    public int getKeepaliveTimeout() {
        return keepaliveTimeout;
    }

    public void setKeepaliveTimeout(int keepaliveTimeout) {
        this.keepaliveTimeout = keepaliveTimeout;
    }

    public boolean isGzip() {
        return gzip;
    }

    public void setGzip(boolean gzip) {
        this.gzip = gzip;
    }

    public List<HttpServer> getServer() {
        return server;
    }

    public void setServer(List<HttpServer> server) {
        this.server = server;
    }
}